import React, { Component } from 'react'
import { Grid } from 'mauerwerk'
import Cell from './Cell'
import data from './data'
import { ReactComponent as ReactLogo } from './images/farm.svg'
import './App.css';
class App extends Component  {
  state = { data, columns: 1, margin: 5, filter: '', height: true, windowWidth:0, windowHeight:0}
  
  updateDimensions = () => {
    let numcolumns=0
    console.log("width:", window.innerWidth )
    if (window.innerWidth<=300){
      numcolumns=1
    } else if (window.innerWidth<=600) {
      numcolumns=2
    } else if (window.innerWidth<=900) {
      numcolumns=3
    } else {
      numcolumns=4
    }
    this.setState({ 
      windowWidth : window.innerWidth, 
      windowHeight : window.innerHeight, 
      columns: numcolumns      
    });
  };

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  render() {
  
    const data = this.state.data.filter(
      d => d.name.toLowerCase().indexOf(this.state.filter) !== -1
    )
    return (
      <div className="main">        
        <header className="App-header">   
          <div className="requadre">
            <div className="fila-centrada">
              <a aria-current="page" className="logo" href="/">                
                <ReactLogo className="app-logo"/>                
                <span className="logo_text">
                  Paissatges
                </span>
              </a>
              <nav className="fila-navegador">
                <a className="item-navegador" href="#joanMiro">Joan Miró</a>
                <a className="item-navegador" href="#mar">Mar</a>
                <a className="item-navegador" href="#cel">Cel</a>
                <a className="item-navegador" href="#boscos">Boscos</a>
              </nav>            
            </div>
          </div>
        </header>
        <Grid
          className="grid"
          // Arbitrary data, should contain keys, possibly heights, etc.
          data={data}
          // Key accessor, instructs grid on how to fet individual keys from the data set
          keys={d => d.name}
          // Can be a fixed value or an individual data accessor
          heights={this.state.height ? d => d.height : 200}
          // Number of columns
          columns={this.state.columns}
          // Space between elements
          margin={this.state.margin}
          // Removes the possibility to scroll away from a maximized element
          lockScroll={false}
          // Delay when active elements (blown up) are minimized again
          closeDelay={400}>
          {(data, maximized, toggle) => (
            <Cell {...data} maximized={maximized} toggle={toggle} />
          )}
        </Grid>
      </div>       
    );
  }
}

export default App;

/*

<div>
      {data.title}
      {open && <div>Opened/maximized content here</div>}
      <button onClick={toggle}>{open ? 'Close' : 'Open'}</button>
    </div>


<Header
          {...this.state}
          search={this.search}
          shuffle={this.shuffle}
          setColumns={this.setColumns}
          setMargin={this.setMargin}
          setHeight={this.setHeight}
          //className="App-header"
        >   




<div>
      <div className=" box">
        
      </div>
      <div className=" box">
        <div id="joanMiro" className="image alçada-inicial">
          <img className="image" src= {require('./images/paissatge1925.jpg')} alt="paissatge1925" />
        </div>
        <div id="mar">
          <img className="image" src= {require('./images/paissatge.jpg')} alt="paissatge 1925" />
          <img className="image" src= {require('./images/paissatge.jpg')} alt="paissatge" />
        </div>
        <div id="cel">
          <img className="image" src= {require('./images/paissatge1925.jpg')} alt="paissatge1925" />
        </div>
        <div id="boscos">
          <img className="image" src= {require('./images/paissatge1925.jpg')} alt="paissatge1925" />
        </div>  
        <footer id="footer">footer</footer>
        <footer>
          <p>Posted by: Hege Refsnes</p>
          <p>Contact information: <a href="mailto:someone@example.com">
          someone@example.com</a>.</p>
          <div>
            Icons made by <a 
                            href="https://www.flaticon.com/authors/freepik" 
                            title="Freepik"
                          >Freepik</a> from <a 
                                              href="https://www.flaticon.com/" 
                                              title="Flaticon"
                                            >www.flaticon.com</a>
          </div>
        </footer>
      </div>
    </div> 

    */