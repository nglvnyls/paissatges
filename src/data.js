export default [
    {
      name: 'Paisatge 1925',
      description: '#a8edea → #fed6e3',
      css: 'linear-gradient(to top, #a8edea 0%, #fed6e3 100%)',
      height: 200,
      quadre: './images2/paisatge-1925.jpg'
    },
    {
      name: 'El Jardí',
      description: '1925',
      css: 'linear-gradient(135deg, #f5f7fa 0%, #c3cfe2 100%)',
      height: 400,
      quadre: './images2/el_jard.jpg'
    },
    {
      name: 'Terra Llaurada',
      description: 'el quadre va ser ...',
      css: 'linear-gradient(120deg, #e0c3fc 0%, #8ec5fc 100%)',
      height: 400,
      quadre: './images2/terra_llaurada.jpg'
    },
    {
      name: 'Hort amb ase',
      description: '#f093fb → #f5576c',
      css: 'linear-gradient(120deg, #f093fb 0%, #f5576c 100%)',
      height: 400,
      quadre: './images2/Hort-amb-ase.png'
    },
    {
      name: 'La casa de la Palmera',
      description: 'La casa de la palmera era una casa a prop...',
      css: 'linear-gradient(-225deg, #E3FDF5 0%, #FFE6FA 100%)',
      height: 400,
      quadre: './images2/la_casa_de_la_palmera.jpg'
    },
    {
      name: 'La Masia',
      description: '#5ee7df → #b490ca',
      css: 'linear-gradient(to top, #5ee7df 0%, #b490ca 100%)',
      height: 400,
      quadre: './images2/la-masia.png'
    },
    {
      name: 'Montroig esglèsia i poble',
      description: '#d299c2 → #fef9d7',
      css: 'linear-gradient(to top, #d299c2 0%, #fef9d7 100%)',
      height: 200,
      quadre: './images2/mont_roig_esglesia_i_poble.jpg'
    },
    {
      name: "Mont-roig,l'església i el Poble 2",
      description: 'el poble de Mont-roig va veure ....',
      css: 'linear-gradient(to top, #ebc0fd 0%, #d9ded8 100%)',
      height: 400,
      quadre: './images2/Mont-roig-l-esglsia-i-el-Poble-2.jpg'
    },
    {
      name: 'Paisatge Català',
      description: '#f6d365 → #fda085',
      css: 'linear-gradient(120deg, #f6d365 0%, #fda085 100%)',
      height: 200,
      quadre: './images2/paisatge-catala.jpg'
    },
    {
      name: 'Paisatge',
      description: 'el paisatge ....',
      css: 'linear-gradient(to top, #96fbc4 0%, #f9f586 100%)',
      height: 400,
      quadre: './images2/paisatge.jpg'
    },
    {
      name: 'Ballarina II',
      description: ' #FFFEFF → #D7FFFE',
      css: 'linear-gradient(-225deg, #FFFEFF 0%, #D7FFFE 100%)',
      height: 200,
      quadre: './images2/ballerina-II.jpg'
    }
  ]
  